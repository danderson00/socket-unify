# @x/socket.unify

`@x/socket` middleware to enable Unify over a socket. 

## Authorisation

The `Unify` feature provides authorisation features to the host. Please see the 
[security guide](/guides/security/1-introduction.md) for information on using this feature.

## Aspects

The `Unify` feature provides the following aspects:

### Property Aspects

#### `userId`

Sets the property to the current user identifier.

### Type Aspects

#### `authenticated`

Requires that the user is authenticated before publishing the message.

#### `userConstraint(expression, failureMessage)`

Requires the provided expression evaluates to a truthy value before allowing the message to be published. The 
expression is passed the current user model.

#### `scopeOwnerOnly(...scopeProperties)`

Requires that the current user owns the scope with the specified properties, or that the scope is not owned.

### Call Aspects

#### `authenticated`

Requires that the user is authenticated before subscribing to the vocabulary.

#### `userConstraint(expression, failureMessage)`

Requires the provided expression evaluates to a truthy value before allowing the user to subscribe. The expression is 
passed the current user model.
 
#### `userScope`

Forces the vocabulary to be scoped by the current userId, along with any other required scope properties.

#### `ownerOnly`

Requires the user to be the owner of the scope specified with a `scoped` aspect. 

#### `scopeOwnerOnly(...scopeProperties)`

Requires the user to be the owner of the scope specified by the provided property names. 

## License

**The MIT License (MIT)**

Copyright © 2022 Dale Anderson

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
documentation files (the “Software”), to deal in the Software without restriction, including without limitation the
rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit
persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
