const feature = require('../../consumer')
const { construct } = require('@x/expressions')

test("vocabulary is added to expressions instance from handshakeData", () => {
  const expressions = construct()
  feature({ expressions })({ log: { child: () => {} } }).initialise({
    handshakeData: {
      unify: {
        vocabulary: [{
          name: 'test',
          defaultValue: 0,
          parameters: [],
          returns: 'stream'
        }]
      }
    }
  })

  expressions.subject().test()
})