const subscriptions = require('../../consumer/subscriptions')
const expressions = require('@x/expressions')

const log = { debug: () => {} }

test("resolver function is called to obtain observable", async () => {
  const resolver = jest.fn(() => expressions.subject())
  const subs = subscriptions(expressions, log)
  const o = await subs.subscribe(resolver, 'key')
  expect(resolver.mock.calls.length).toBe(1)
  expect(o.disconnect).toBeInstanceOf(Function)
})

test("resolver results are cached per query and scope", async () => {
  const resolver = jest.fn(() => expressions.subject())
  const subs = subscriptions(expressions, log)
  const o1 = await subs.subscribe(resolver, 'key')
  const o2 = await subs.subscribe(resolver, 'key')
  expect(resolver.mock.calls.length).toBe(1)
  expect(o1.parent).toBe(o2.parent)
})

test("cached observables are released when all connections are released", async () => {
  const disconnect = jest.fn()
  const resolver = jest.fn(() => {
    const o = expressions.subject()
    o.disconnect = disconnect
    return o
  })
  const subs = subscriptions(expressions, log)
  const o1 = await subs.subscribe(resolver, 'key')
  const o2 = await subs.subscribe(resolver, 'key')
  o1.disconnect()
  const o3 = await subs.subscribe(resolver, 'key')
  expect(resolver.mock.calls.length).toBe(1)
  o2.disconnect()
  o3.disconnect()
  expect(disconnect.mock.calls.length).toBe(1)
  const o4 = await subs.subscribe(resolver, 'key')
  expect(resolver.mock.calls.length).toBe(2)
  o4.disconnect()
  expect(disconnect.mock.calls.length).toBe(2)
})

test("immediately subsequent subscriptions do not cause multiple resolver requests", () => {
  const resolver = jest.fn(() => expressions.subject())
  const subs = subscriptions(expressions, log)
  subs.subscribe(resolver, 'key1')
  subs.subscribe(resolver, 'key1')
  subs.subscribe(resolver, 'key2')
  expect(resolver.mock.calls.length).toBe(2)
})

test("failed subscriptions are released", async () => {
  let firstCall = true
  const resolver = jest.fn(() => {
    if(firstCall) {
      firstCall = false
      return Promise.reject(new Error('failed'))
    } else {
      return Promise.resolve(expressions.subject())
    }
  })
  const subs = subscriptions(expressions, log)
  await expect(subs.subscribe(resolver, 'key1')).rejects.toMatchObject({ message: 'failed' })
  await subs.subscribe(resolver, 'key1')
})