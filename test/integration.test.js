const hostModule = require('@x/socket/host')
const hostFeature = require('../host')
const consumerModule = require('@x/socket/consumer')
const consumerFeature = require('../consumer')
const { construct } = require('@x/expressions')
const WebSocket = require('ws')

let server

const setup = async vocabulary => {
  const hostExpressions = construct()
  server = new WebSocket.Server({ port: 1234 })
  const host = await hostModule({ server, log: { level: 'fatal' } })
    .useFeature(hostFeature({ expressions: hostExpressions, vocabulary }))

  const consumerExpressions = construct()
  const consumer = await consumerModule({ url: 'ws://localhost:1234' })
    .useFeature(consumerFeature({ expressions: consumerExpressions }))
    .connect()

  return { consumer, consumerExpressions, host, server }
}

afterEach(() => server.close())

test("vocabulary is created on the consumer", async () => {
  const { consumerExpressions } = await setup({ getSum: o => o.sum() })
  expect(consumerExpressions.subject().getSum()()).toBe(0)
})

test("vocabulary can be executed on the host", async () => {
  const { consumer } = await setup({
    innerCount: o => o.count(),
    getCount: o => o.innerCount()
  })
  const o = await consumer.subscribe({
    type: 'vocabulary',
    name: 'getCount'
  })
  expect(o()).toBe(0)
  await consumer.publish({})
  expect(o()).toBe(1)
})

test("vocabulary can be executed on the host as raw expression", async () => {
  const { consumer, consumerExpressions } = await setup({ getCount: o => o.count() })
  const definition = consumerExpressions.extractDefinition(o => o.getCount())
  const o = await consumer.subscribe({ type: 'raw', definition })
  expect(o()).toBe(0)
  await consumer.publish({})
  expect(o()).toBe(1)
})

test("parameterised vocabulary can be executed on the host as raw expression", async () => {
  const { consumer, consumerExpressions } = await setup({ pluck: (o, property) => o.select(property) })
  const o = await consumer.subscribe({
    type: 'raw',
    definition: consumerExpressions.extractDefinition(o => o.pluck('value'))
  })
  await consumer.publish({ value: 1 })
  expect(o()).toBe(1)
})