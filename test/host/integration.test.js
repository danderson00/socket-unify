const hostModule = require('@x/socket/host')
const feature = require('../../host')
const { construct } = require('@x/expressions')
const WebSocket = require('ws')

let client, host, server, sentFromHost

const openSocket = () => new Promise(resolve => {
  const socket = new WebSocket('ws://localhost:1234')
  socket.on('open', () => resolve(socket))
})

const setup = async vocabulary => {
  const expressions = construct()
  expressions.addVocabulary(vocabulary)
  server = new WebSocket.Server({ port: 1234 })
  host = hostModule({ server, log: { level: 'fatal' } })
    .useFeature(feature({ expressions }))
  client = await openSocket()
  sentFromHost = jest.fn()
  client.on('message', sentFromHost)
}

const delay = delay => new Promise(r => setTimeout(r, delay))

afterEach(() => server.close())

test("handshake returns vocabulary", async () => {
  await setup({
    sumOfValues: o => o.sum(),
    oddsAndEvens: o => o.groupBy(x => x % 2)
  })

  client.send(JSON.stringify({
    sessionId: 1,
    session: 'establish',
    type: 'handshake',
    data: { version: '0.0.1' },
    commandId: 1
  }))
  await delay(10)

  expect(sentFromHost.mock.calls).toEqual([
    [JSON.stringify({ commandId: 1, status: 'ack' })],
    [JSON.stringify({
      status: 'ok',
      data: {
        operations: [
          { name: 'subscribe' },
          { name: 'subscribeMachine' },
          { name: 'publish' }
        ],
        unify: {
          vocabulary: [
            {
              name: 'sumOfValues',
              defaultValue: 0,
              parameters: [],
              returns: 'stream'
            },
            {
              name: 'oddsAndEvens',
              defaultValue: [],
              parameters: [],
              returns: 'aggregate'
            },
            {
              name: 'scopeOwners',
              defaultValue: [],
              parameters: ['scopeProps', 'userIdProp'],
              returns: 'aggregate'
            }
          ]
        }
      },
      session: 'terminate',
      sessionId: 1
    })]
  ])
})
