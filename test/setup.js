const hostModule = require('@x/socket/host')
const hostFeature = require('../host')
const consumerModule = require('@x/socket/consumer')
const consumerFeature = require('../consumer')
const { construct } = require('@x/expressions')
const WebSocket = require('ws')

module.exports = {
  integration: async (options, test) => {
    const server = new WebSocket.Server({ port: 1234 })

    const closeServer = error => {
      return new Promise(
        (resolve, reject) => server.close(() => {
          error ? reject(error) : resolve()
        })
      )
    }

    try {
      const host = hostModule({ server, log: { level: 'fatal' } })
        .useApi({
          authenticate: user => ({ success: true, user }),
          logout: () => {
          }
        })
        .use({
          authenticate: ({ next, connection }, user = { id: 'userId' }) => (connection.user = user, next()),
          logout: ({ next, connection }) => (connection.user = undefined, next())
        })
        .useFeature(hostFeature({
          expressions: construct(),
          scopes: ['userId'],
          strictTypes: true,
          strictApi: true,
          ...options
        }))

      const consumer = await consumerModule({ url: 'ws://localhost:1234' })
        .useFeature(consumerFeature())
        .connect()

      return Promise.resolve(test({ consumer, host, server }))
        .then(() => closeServer())
        .catch(closeServer)
    } catch(error) {
      return closeServer(error)
    }
  }
}
