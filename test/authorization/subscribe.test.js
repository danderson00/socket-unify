const hostModule = require('@x/socket/host')
const hostFeature = require('../../host')
const consumerModule = require('@x/socket/consumer')
const consumerFeature = require('../../consumer')
const { construct } = require('@x/expressions')
const WebSocket = require('ws')

let server

const setup = async ({ vocabulary, user = { id: 'userId' } } = {}) => {
  server = new WebSocket.Server({ port: 1234 })
  const host = hostModule({ server, log: { level: 'fatal' } })
    .useApi({ authenticate: () => ({ success: true, user }), logout: () => {} })
    .use({ subscribe: ({ next, connection }) => (connection.user = user, next()) })
    .useFeature(hostFeature({
      expressions: construct(),
      scopes: ['userId'],
      vocabulary,
      strictApi: true
    }))

  const consumer = await consumerModule({ url: 'ws://localhost:1234' })
    .useFeature(consumerFeature())
    .connect()

  return { consumer, host, server }
}

afterEach(() => server && server.close())

describe("authenticated aspect", () => {
  test("rejects if user is not authenticated", async () => {
    const { consumer } = await setup({
      vocabulary: ({ authenticated }) => ({
        authenticated: [authenticated, o => o],
      }),
      user: false
    })
    await expect(consumer.subscribe({ type: 'vocabulary', name: 'authenticated' }))
      .rejects.toMatchObject({ message: 'You must be authenticated to subscribe to authenticated' })
  })

  test("has no effect if user is authenticated", async () => {
    const { consumer } = await setup({
      vocabulary: ({ authenticated }) => ({
        authenticated: [authenticated, o => o]
      })
    })
    await consumer.subscribe({ type: 'vocabulary', name: 'authenticated' })
  })
})

describe("userConstraint aspect", () => {
  const vocabulary = (withUser = true) => ({ scoped, userConstraint }) => ({
    ...(withUser && {
      user: [scoped('userId'), o => o.topic('reputation').count()]
    }),
    userConstraint: [userConstraint(
      reputation => reputation > 0,
      "You must have at least 1 reputation"
    ), o => o]
  })

  test("rejects if user is not authenticated", async () => {
    const { consumer } = await setup({ vocabulary: vocabulary(), user: false })
    await expect(consumer.subscribe({ type: 'vocabulary', name: 'userConstraint' }))
      .rejects.toMatchObject({ message: 'You must have at least 1 reputation' })
  })

  test("rejects if user vocabulary is not defined", async () => {
    const { consumer } = await setup({ vocabulary: vocabulary(false) })
    await expect(consumer.subscribe({ type: 'vocabulary', name: 'userConstraint' }))
      .rejects.toMatchObject({ message: 'You must have at least 1 reputation' })
  })

  test("simple constraint", async () => {
    const { consumer } = await setup({ vocabulary: vocabulary() })
    await consumer.authenticate()
    await expect(consumer.subscribe({ type: 'vocabulary', name: 'userConstraint' }))
      .rejects.toMatchObject({ message: 'You must have at least 1 reputation' })

    await consumer.publish({ topic: 'reputation', userId: 'anotherUserId' })
    await expect(consumer.subscribe({ type: 'vocabulary', name: 'userConstraint' }))
      .rejects.toMatchObject({ message: 'You must have at least 1 reputation' })

    await consumer.publish({ topic: 'reputation', userId: 'userId' })
    await consumer.subscribe({ type: 'vocabulary', name: 'userConstraint' })
  })
})

test("userScope aspect", async () => {
  const { consumer } = await setup({
    vocabulary: ({ userScope }) => ({
      userScope: [userScope, o => o.count()]
    })
  })
  await consumer.publish({ userId: 'userId' })
  await consumer.publish({ userId: 'userId' })
  await consumer.publish({ userId: 'userId2' })
  const result = await consumer.subscribe({ type: 'vocabulary', name: 'userScope' })
  expect(result()).toBe(2)
})

test("user vocabulary subscription updates when user authentication changes", async () => {
  const { consumer } = await setup({
    vocabulary: ({ open }) => ({
      user: [open, o => o.count()]
    })
  })
  await consumer.publish({ userId: 'userId' })
  await consumer.publish({ userId: 'userId' })
  await consumer.publish({ userId: 'userId2' })
  const result = await consumer.subscribe({ type: 'vocabulary', name: 'user' })
  expect(result()).toBe(undefined)
  await consumer.authenticate()
  expect(result()).toBe(2)
  await consumer.logout()
  expect(result()).toBe(undefined)
})
