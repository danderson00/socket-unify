const { integration } = require('../setup')

const config =   {
  vocabulary: ({ scoped, scopeOwnerOnly, ownerOnly }) => ({
    ownerOnly: [ownerOnly, o => o.count()],
    scopeOwnerOnly: [scoped('p1', 'p2'), scopeOwnerOnly('p1'), o => o.count()]
  }),
  types: ({ required, scopeOwnerOnly, userId }) => ({
    owned: [scopeOwnerOnly('p1'), {
      p1: [required, Number],
      userId
    }]
  }),
  scopes: ['p1', 'p2'],
  ownedScopes: ['p1', 'p2']
}

test("basic integration test", () => integration(config,
  async ({ consumer }) => {
    await expect(consumer.subscribe({ type: 'vocabulary', name: 'ownerOnly' }, { p1: 1 })).rejects.toMatchObject({
      message: "You are currently not authorized to subscribe to ownerOnly"
    })

    await consumer.authenticate({ id: 'user1' })
    await consumer.publish({ topic: 'owned', p1: 1 })
    const o = await consumer.subscribe({ type: 'vocabulary', name: 'ownerOnly' }, { p1: 1 })
    expect(o()).toBe(1)
    o.disconnect()

    await consumer.authenticate({ id: 'user2' })
    await expect(consumer.subscribe({ type: 'vocabulary', name: 'ownerOnly' }, { p1: 1 })).rejects.toMatchObject({
      message: "You are currently not authorized to subscribe to ownerOnly"
    })
    await expect(consumer.publish({ topic: 'owned', p1: 1 })).rejects.toMatchObject({
      message: "You are currently not authorized to publish owned"
    })
  }
))

test("different scope owner", () => integration(config,
  async ({ consumer }) => {
    await expect(consumer.subscribe({ type: 'vocabulary', name: 'scopeOwnerOnly' }, { p1: 1, p2: 2 })).rejects.toMatchObject({
      message: "You are currently not authorized to subscribe to scopeOwnerOnly"
    })

    await consumer.authenticate({ id: 'user1' })
    await expect(consumer.subscribe({ type: 'vocabulary', name: 'scopeOwnerOnly' }, { p1: 1, p2: 2 })).rejects.toMatchObject({
      message: "You are currently not authorized to subscribe to scopeOwnerOnly"
    })

    await consumer.publish({ topic: 'owned', p1: 1 })
    await consumer.subscribe({ type: 'vocabulary', name: 'scopeOwnerOnly' }, { p1: 1, p2: 2 })
  }
))