const scopesModule = require('../../host/authorization/scopes')
const vocabulary = require('../../host/authorization/vocabulary')
const hostModule = require('@x/unify.host')
const { construct } = require('@x/expressions')

const setup = async () => {
  const host = hostModule({
    expressions: construct(),
    scopes: ['p1', 'p2'],
    vocabulary,
    log: { level: 'none' }
  })

  const scopes = scopesModule(host, [['p1'], ['p2'], ['p1', 'p2']])
  await scopes.initialized

  return { host, scopes }
}

test("owners are set from first publish", async () => {
  const { scopes, host } = await setup()

  expect(scopes.ownerFor({ p1: '1' })).toBe(undefined)
  await(host.publish({ p1: '1', userId: 'user' }))
  await(host.publish({ p1: '1', userId: 'user2' }))
  await(host.publish({ p1: '2', userId: 'user2' }))
  expect(scopes.ownerFor({ p1: '1' })).toBe('user')
  expect(scopes.ownerFor({ p1: '2' })).toBe('user2')
})

test("messages without scope properties are ignored", async () => {
  const { scopes, host } = await setup()

  await(host.publish({ userId: 'user' }))
  expect(scopes.ownerFor({ p1: undefined })).toBe(undefined)
})

test("multiple scopes", async () => {
  const { scopes, host } = await setup()

  await(host.publish({ p1: 1, userId: 'user1' }))
  await(host.publish({ p2: 2, userId: 'user2' }))
  await(host.publish({ p1: 1, p2: 2, userId: 'user3' }))
  await(host.publish({ p2: 3, userId: 'user4' }))
  await(host.publish({ p1: 1, p2: 3, userId: 'user5' }))

  await(host.publish({ p1: 1, userId: 'next' }))
  await(host.publish({ p2: 2, userId: 'next' }))
  await(host.publish({ p1: 1, p2: 2, userId: 'next' }))
  await(host.publish({ p2: 3, userId: 'next' }))
  await(host.publish({ p1: 1, p2: 3, userId: 'next' }))

  expect(scopes.ownerFor({ p1: 1 })).toBe('user1')
  expect(scopes.ownerFor({ p2: 2 })).toBe('user2')
  expect(scopes.ownerFor({ p1: 1, p2: 2 })).toBe('user3')
  expect(scopes.ownerFor({ p2: 3 })).toBe('user4')
  expect(scopes.ownerFor({ p1: 1, p2: 3 })).toBe('user5')
})