const hostModule = require('@x/socket/host')
const hostFeature = require('../../host')
const consumerModule = require('@x/socket/consumer')
const consumerFeature = require('../../consumer')
const { construct } = require('@x/expressions')
const WebSocket = require('ws')

let server

const setup = async ({ types, vocabulary, user = { id: 'userId' } }) => {
  server = new WebSocket.Server({ port: 1234 })
  const host = hostModule({ server, log: { level: 'fatal' } })
    .useApi({ authenticate: () => ({ success: true, user })})
    .use({ publish: ({ next, connection }) => (connection.user = user, next()) })
    .useFeature(hostFeature({
      types,
      vocabulary,
      expressions: construct(),
      scopes: ['userId'],
      strictTypes: true
    }))

  const consumer = await consumerModule({ url: 'ws://localhost:1234' })
    .useFeature(consumerFeature())
    .connect()

  return { consumer, host, server }
}

afterEach(() => server.close())

describe("userId aspect", () => {
  test("sets property to authenticated user id", async () => {
    const types = ({ userId }) => ({ simpleUserId: { userId } })
    const { consumer } = await setup({ types })
    const o = await consumer.publish({ topic: 'simpleUserId' })
    expect(o.message).toEqual({ topic: 'simpleUserId', userId: 'userId' })
  })

  test("sets property to undefined when not authenticated", async () => {
    const types = ({ userId }) => ({ simpleUserId: { userId } })
    const { consumer } = await setup({ types, user: false })
    const o = await consumer.publish({ topic: 'simpleUserId' })
    expect(o.message).toEqual({ topic: 'simpleUserId', userId: undefined })
  })
})

describe("authenticated aspect", () => {
  test("rejects if user is not authenticated", async () => {
    const types = ({ authenticated }) => ({ mustAuthenticate: [authenticated, { id: String }] })
    const { consumer } = await setup({ types, user: false })
    await expect(consumer.publish({ topic: 'mustAuthenticate', id: '1' }))
      .rejects.toMatchObject({ message: 'You must be authenticated to publish mustAuthenticate' })
  })

  test("has no effect if user is authenticated", async () => {
    const types = ({ authenticated }) => ({ mustAuthenticate: [authenticated, { id: String }] })
    const { consumer } = await setup({ types })
    await consumer.publish({ topic: 'mustAuthenticate', id: '1' })
  })
})

describe("userConstraint aspect", () => {
  test("rejects if user is not authenticated", async () => {
    const types = ({ userConstraint }) => ({ userConstraint: [userConstraint(), { id: String }] })
    const { consumer } = await setup({ types, user: false })
    await expect(consumer.publish({ topic: 'userConstraint', id: '1' }))
      .rejects.toMatchObject({ message: 'You are currently not authorized to publish userConstraint' })
  })

  test("rejects if user vocabulary is not defined", async () => {
    const types = ({ userConstraint }) => ({ userConstraint: [userConstraint(), { id: String }] })
    const { consumer } = await setup({ types })
    await expect(consumer.publish({ topic: 'userConstraint', id: '1' }))
      .rejects.toMatchObject({ message: 'You are currently not authorized to publish userConstraint' })
  })

  test("simple constraint", async () => {
    const vocabulary = {
      user: o => o.topic('reputation').count()
    }
    const types = ({ userConstraint }) => ({
      userConstraint: [
        userConstraint(reputation => reputation > 0, "You must have at least 1 reputation"),
        { id: String }
      ],
      reputation: { userId: String }
    })
    const { consumer } = await setup({ vocabulary, types })
    await consumer.authenticate()
    await expect(consumer.publish({ topic: 'userConstraint', id: '1' }))
      .rejects.toMatchObject({ message: 'You must have at least 1 reputation' })

    await consumer.publish({ topic: 'reputation', userId: 'anotherUserId' })
    await expect(consumer.publish({ topic: 'userConstraint', id: '1' }))
      .rejects.toMatchObject({ message: 'You must have at least 1 reputation' })

    await consumer.publish({ topic: 'reputation', userId: 'userId' })
    await consumer.publish({ topic: 'userConstraint', id: '1' })
  })
})