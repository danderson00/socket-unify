module.exports = {
  scopeOwners: (o, scopeProps, userIdProp) => o
    .where(x => x[userIdProp] && scopeProps.every(prop => x.hasOwnProperty(prop)))
    .groupBy(
      x => scopeProps.reduce(
        (values, prop) => ({ ...values, [prop]: x[prop] }),
        {}
      ),
      (o, key) => o.compose(
        o => o.first().select(userIdProp),
        owner => ({ scope: key, owner })
      )
    )
}