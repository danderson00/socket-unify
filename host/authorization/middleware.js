const { proxy, subject, swappable } = require('@x/expressions')

module.exports = (host, vocabulary, scopes) => {
  const userVocabularyDefined = !!vocabulary.find(x => x.name === 'user')

  const ensureUserModelInitialised = connection => {
    if(!connection.observables('userModel')) {
      connection.observables.attach('userModel', swappable(undefined, { publishOnSwap: true }))
    }
    return connection.observables('userModel')
  }

  const setUserModel = async ({ connection, next }) => {
    const result = await next()

    if(userVocabularyDefined) {
      const userModelObservable = ensureUserModelInitialised(connection)
      userModelObservable.disconnect()

      if (result.success) {
        userModelObservable.swap(await host.subscribe(
          { type: 'vocabulary', name: 'user' },
          { userId: result.user.id },
          { context: { user: result.user } }
        ))
      } else {
        userModelObservable.swap(subject())
      }
    }

    return result
  }

  const logout = ({ connection, next }) => {
    const userModelObservable = ensureUserModelInitialised(connection)
    userModelObservable.disconnect()
    userModelObservable.swap(subject())
    return next()
  }

  // this overwrites any context passed from the consumer to ensure the consumer can't arbitrarily set the user!
  const setPublishContext = async ({ next, connection }, message) => {
    await scopes.initialized

    return next(message, {
      user: connection.user,
      userModel: ensureUserModelInitialised(connection),
      ownerFor: scopes.ownerFor
    })
  }

  const setSubscribeContext = async ({ next, connection }, request, scope, options) => {
    if(request.type === 'vocabulary' && request.name === 'user') {
      return proxy(ensureUserModelInitialised(connection))
    }

    await scopes.initialized

    return next(request, scope, {
      ...options,
      context: {
        user: connection.user,
        userModel: ensureUserModelInitialised(connection),
        ownerFor: scopes.ownerFor,
        scopeOwner: scopes.ownerFor(scope)
      }
    })
  }

  return {
    publish: setPublishContext,
    subscribe: setSubscribeContext,
    authenticate: setUserModel,
    createUser: setUserModel,
    logout,

    // eventually we'll pass user data to the userModel expression
    // updateUserData: ({ connection, next }) => {},
    // verifyUser: ({ connection, next }) => {}
  }
}