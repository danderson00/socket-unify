const middlewareModule = require('./middleware')
const scopesModule = require('./scopes')

module.exports = (api, vocabulary, options) => {
  const scopes = scopesModule(api, options.ownedScopes, options.userId)
  return {
    middleware: middlewareModule(api, vocabulary, scopes)
  }
}