const { unwrap } = require('@x/expressions')

module.exports = {
  propertyAspects: {
    userId: (value, property, { context }) => ({ newValue: context.user && context.user.id })
  },
  typeAspects: {
    authenticated: ({ topic = 'that message' } = {}, { context }) => ({
      success: !!(context.user && context.user.id),
      message: `You must be authenticated to publish ${topic}`
    }),
    userConstraint: (expression = o => o, message) => (
      function userConstraint({ topic = 'that message' } = {}, { context }) {
        return {
          success: !!(context.user && context.userModel && expression(unwrap(context.userModel))),
          message: message || `You are currently not authorized to publish ${topic}`
        }
      }
    ),
    scopeOwnerOnly: (...props) => (
      function scopeOwnerOnly(message, { context }) {
        const owner = context.ownerFor(pluckScopeFrom(props, message))

        return {
          success: owner === undefined || (context.user && owner === context.user.id),
          message: `You are currently not authorized to publish ${message && message.topic || 'that message'}`
        }
      }
    )
  },
  callAspects: {
    authenticated: {
      validate: ({ context }) => !!(context.user && context.user.id) ||
        `You must be authenticated to subscribe to ${context.name}`,
      context: () => ({ public: true })
    },
    userConstraint: (expression, message) => ({
      validate: ({ context }) => {
        return !!(context.user && context.userModel && expression(unwrap(context.userModel))) ||
          message || `You are currently not authorized to subscribe to ${context.name}`
      },
      context: () => ({ public: true })
    }),
    userScope: ({ next, parameters: [request, scope, options], context }) => {
      return next({
        parameters: [request, { ...scope, userId: context.user ? context.user.id : null }, options],
        context: { public: true }
      })
    },
    ownerOnly: {
      validate: ({ context }) => (
        (context.user && context.scopeOwner === context.user.id) ||
        `You are currently not authorized to subscribe to ${context.name}`
      ),
      context: () => ({ public: true })
    },
    scopeOwnerOnly: (...props) => ({
      validate: ({ context, parameters: [, scope = {}] }) => (
        (context.user && context.ownerFor(pluckScopeFrom(props, scope)) === context.user.id) ||
        `You are currently not authorized to subscribe to ${context.name}`
      )
    })
  }
}

const pluckScopeFrom = (props, from) => props.reduce(
  (scope, prop) => ({ ...scope, [prop]: from[prop] }),
  {}
)