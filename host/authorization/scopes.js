const { deepEqual, unwrap } = require('@x/expressions/src/utilities')
const { forceArray } = require('../utilities')

module.exports = (api, ownedScopes = [], userIdProp = 'userId') => {
  const owners = {}

  // socket currently has no async feature initialization - expose promise to delay requests
  const initialized = Promise.all(ownedScopes.map(scopeProps => {
    scopeProps = forceArray(scopeProps)

    return api
      .subscribe({
        type: 'vocabulary',
        name: 'scopeOwners',
        parameters: [scopeProps, userIdProp]
      },
      {},
      { // this is a total hack - we need to move type validation to socket.types
        context: { public: true }
      })
      .then(scopeOwners => {
        owners[JSON.stringify(scopeProps.sort())] = scopeOwners
      })
    }
  ))

  return {
    initialized,
    ownerFor(scope) {
      if(scope && Object.keys(scope).length > 0) {
        const scopeOwners = owners[JSON.stringify(Object.keys(scope).sort())]
        const owner = scopeOwners && unwrap(scopeOwners).find(x => deepEqual(x.scope, scope))
        return owner && owner.owner
      }
    }
  }
}