module.exports = {
  forceArray: source => source instanceof Array ? source : [source]
}