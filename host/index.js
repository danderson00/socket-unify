const expressions = require('@x/expressions')
const createHost = require('@x/unify.host')
const authorization = require('./authorization')
const aspects = require('./authorization/aspects')
const vocabulary = require('./authorization/vocabulary')
const { forceArray } = require('./utilities')

module.exports = (userOptions = {}) => {
  const options = {
    expressions: userOptions.expressions || expressions.construct(),
    ...userOptions,
    // logger: (userOptions.logger || log).child({ source: 'socket.unify.host' }),
    aspects: { ...aspects, ...userOptions.aspects },
    vocabulary: [vocabulary, ...(forceArray(userOptions.vocabulary || []))]
  }

  // it is questionable that the host should be created here rather than passed in
  // creating externally would also solve the problem of exposing the host below
  // so that purgeExpressionCache can be called by the bundle watcher
  // this would mean the aspects and vocabulary that is part of authorization
  // would need to be exported and added manually to the host configuration
  const host = createHost(options)

  const featureConstructor = ({ log }) => {
    // TODO: hiding internal functions needs a better solution
    const api = {
      subscribe: host.subscribe,
      subscribeMachine: host.subscribeMachine,
      publish: host.publish
    }

    // TODO: remove non-public vocabulary from handshake data (when in production mode)
    const shallowVocabulary = options.expressions.getVocabularyShallow()
    const auth = authorization(host, shallowVocabulary, options)

    return {
      name: 'unify',
      api,
      middleware: auth.middleware,
      handshake: () => ({
        scopes: options.scopes,
        vocabulary: shallowVocabulary
      })
    }
  }
  // this is a rather nasty way of exposing the host so the bundle can purge the expression cache on restart
  // perhaps a better solution is to create the host in the bundle and pass it in to this feature
  // (requires adding authorization vocabulary after host creation)
  featureConstructor.host = host
  return featureConstructor
}
