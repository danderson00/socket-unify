const subscriptionsModule = require('./subscriptions')
const vocabulary = require('./vocabulary')
const join = require('./join')
const expressionsModule = require('@x/expressions')

module.exports = ({ expressions = expressionsModule } = {}) => ({ log }) => ({
  name: 'unify',
  initialise: ({ handshakeData }) => {
    log = log.child({ source: 'socket.unify.consumer' })
    expressions.addOperator(join(expressions))

    if (handshakeData && handshakeData.unify) {
      handshakeData.unify.vocabulary.forEach(
        shallow => expressions.addOperator(vocabulary(expressions, shallow))
      )

      // this is not ideal and will be replaced by the system information host observable
      expressions.scopes = handshakeData.unify.scopes
    }

    const subscriptions = subscriptionsModule(expressions, log)

    return {
      middleware: {
        subscribe: ({ next/*, disconnect*/ }, request, scope) => {
          // TODO: need to manually disconnect the session if it doesn't actually hit the server
          return subscriptions.subscribe(next, request, scope)
        }
      }
    }
  }
})
