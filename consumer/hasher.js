const hasher = require('string-hash')

module.exports = definition => hasher(JSON.stringify(definition)).toString()