// this is a cut down version of the @x/unify.host join component just to create the appropriate definition
// to send to the host. It would not be too difficult to use the actual join component and hook it up
// to the consumer subscriptions module, but this is not needed now (ever?)
module.exports = expressions => ({
  type: 'stream',
  identifier: 'join',
  component: () => ({
    create: (scopeProperty, expression = o => o) => {
      return expressions.serializable.subject({
        initialValue: expressions.unwrap(expression(expressions.subject())),
        getDefinition: () => ({
          type: 'stream',
          identifier: 'join',
          returns: expressions.extractReturnType(expression),
          scopeProperty,
          expression
        })
      })
    }
  })
})
