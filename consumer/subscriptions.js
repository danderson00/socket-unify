const hasher = require('./hasher')

module.exports = (expressionsModule, log) => {
  const expressions = {}

  const resolveExpression = (resolver, key, debugInfo) => {
    const expression = expressions[key] = expressions[key] || {
      promise: resolver(),
      subscriberCount: 0
    }
    expression.subscriberCount++
    log.debug(`Incremented subscriber count`, { [key]: expression.subscriberCount, ...debugInfo })
    return expression
  }

  const releaseExpression = (observable, expression, key, debugInfo) => {
    expression.subscriberCount--
    log.debug(
      expression.subscriberCount === 0 ? 'Destroyed subscription' : `Decremented subscriber count`,
      { [key]: expression.subscriberCount, ...debugInfo }
    )
    if(expression.subscriberCount === 0) {
      observable && observable.disconnect()
      delete expressions[key]
    }
  }

  return {
    subscribe: async (resolver, request, scope) => {
      const debugInfo = {
        ...(request.type === 'vocabulary' && { vocabulary: request.name }),
        scope
      }
      const key = hasher({ request, scope })
      
      const expression = resolveExpression(resolver, key, debugInfo)
      try {
        const observable = await expression.promise

        // a little nasty, monkey patch disconnect to reduce the subscriber count
        const proxy = expressionsModule.proxy(observable)
        const disconnect = proxy.disconnect
        proxy.disconnect = () => {
          disconnect()
          releaseExpression(observable, expression, key, debugInfo)
        }

        return proxy
      } catch(error) {
        releaseExpression(undefined, expression, key, debugInfo)
        throw error
      }
    }
  }
}