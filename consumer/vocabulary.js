module.exports = (expressions, shallow) => ({
  type: 'stream',
  identifier: shallow.name,
  component: inputObservable => ({
    create: (...args) => {
      return expressions.subject({
        initialValue: shallow.defaultValue,
        getDefinition: () => ({
          type: 'stream',
          identifier: shallow.name,
          returns: shallow.returns,
          parameters: args,
          executionContext: {
            closures: shallow.parameters.reduce(
              (closures, name, index) => ({ ...closures, [name]: args[index] }),
              {}
            )
          }
        })
      })
    }
  })
})